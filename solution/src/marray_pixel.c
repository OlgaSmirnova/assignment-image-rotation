//
// Created by etoxto on 28.12.2021.
//

#include "marray_pixel.h"
#include "malloc.h"
#include "pixel.h"

const struct optional_marray_pixel NONE_MARRAY_PIXEL = {NULL, false };

struct optional_marray_pixel marray_pixel_init (size_t w, size_t h) {
    struct marray_pixel* marray = malloc(sizeof (struct marray_pixel));
    if (marray) {
        marray->data = malloc(sizeof(struct array_pixel) * w);
        if (marray->data) {
            for (size_t i = 0; i < w; ++i) {
                struct optional_array_pixel opt_pixel = array_pixel_init(h);
                if (opt_pixel.valid) {
                    marray->data[i] = opt_pixel.value;
                    marray->size = h;
                } else {
                    for (size_t j = 0; j < i - 1; ++j) {
                        array_pixel_free(&marray->data[i]);
                    }
                    free(marray->data);
                    free(marray);
                    return NONE_MARRAY_PIXEL;
                }
            }

            marray->size = w;
            return some_marray_pixel(marray);
        } else {
            free(marray);
            return NONE_MARRAY_PIXEL;
        }
    }

    return NONE_MARRAY_PIXEL;
}

struct optional_pixel marray_pixel_get (struct marray_pixel a, size_t i, size_t j ) {
    if (!a.data || a.size == 0 || i  > a.size || a.data[i].size == 0)
        return NONE_PIXEL;

    return array_pixel_get(a.data[i], j);
}

bool marray_pixel_set (struct marray_pixel a, size_t i, size_t j, struct pixel value) {
    if (a.size == 0 || a.data[i].size == 0)
        return false;

    a.data[i].data[j] = value;
    return true;
}

void marray_pixel_free (struct marray_pixel* array) {
    for (size_t i = 0; i < array->size; ++i) {
        array_pixel_free(&array->data[i]);
    }
    free(array->data);
    free(array);
    array = NULL;
}

struct optional_marray_pixel some_marray_pixel (struct marray_pixel* array) {
    return (struct optional_marray_pixel) {.value = array, .valid = true};
}
