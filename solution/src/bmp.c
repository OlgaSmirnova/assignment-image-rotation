//
// Created by etoxto on 28.12.2021.
//

#include "bmp.h"

#include <stdbool.h>

#define B_OFF_BITS 54
#define BI_SIZI 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24

static bool bmp_signature_check (const struct bmp_header* header) {
    unsigned char bytes[2];
    bytes[0] = header->bfSignature & 0xFF;
    bytes[1] = (header->bfSignature >> 8) & 0xFF;
    if (bytes[0] == 'B' && bytes[1] == 'M') {
        return true;
    }

    return false;
}

static bool bmp_to_image (FILE* file, const struct bmp_header* header, struct image** img) {
    *img = image_init(header->biWidth, header->biHeight);
    int32_t padding = (((header->biWidth * 3) % 4) ? 4 - (header->biWidth * 3) % 4 : 0);
    for (int32_t i = 0; i < header->biHeight; ++i) {
        for (int32_t j = 0; j < header->biWidth; ++j) {
            struct pixel pixel = {0};
            if (!fread(&pixel, sizeof (struct pixel), 1, file)) {
                return false;
            }
            image_set_pixel_by_coordinates(*img, header->biHeight - i - 1, j, &pixel);
        }
        if (fseek(file, padding, SEEK_CUR)) return false;
    }

    return true;
}

static struct bmp_header bmp_header_construct (struct image const* img) {
    struct bmp_header header = {0};
    uint32_t fileSize   = sizeof (struct bmp_header)
                          + 3 * sizeof (uint8_t) * (img->height * img->width)
                          + ((img->width % 4) ? (img->height + 1) * (4 - (img->width % 4)) : 0);
    header.bfSignature  = ('M' << 8) + 'B';
    header.bOffBits     = B_OFF_BITS;
    header.biSize       = BI_SIZI;
    header.biWidth      = img->width;
    header.biHeight     = img->height;
    header.biPlanes     = BI_PLANES;
    header.biBitCount   = BI_BIT_COUNT;
    header.bfileSize    = fileSize;
    header.biSizeImage  = fileSize - sizeof (struct bmp_header);

    return header;
}

enum read_status from_bmp (FILE* in, struct image** img) {
    struct bmp_header header = {0};
    if (in) {
        if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
            return READ_INVALID_HEADER;
        }

        if (!bmp_signature_check(&header)) {
            return READ_INVALID_SIGNATURE;
        }

        if (header.biWidth <= 0 || header.biHeight <= 0) {
            return READ_INVALID_BITS;
        }

        if (!bmp_to_image(in, &header, img)) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

static bool write_array(struct bmp_header header, FILE* out){
        int8_t zero = 0;
        if ((header.biWidth * 3) % 4 != 0) {
            for (size_t k = 0; k < 4 - (header.biWidth * 3) % 4; ++k)
                if (!fwrite(&zero, sizeof (int8_t), 1, out)) return false;
        }
        return true;
    }

enum write_status to_bmp (FILE* out, struct image const* img) {
    struct bmp_header header = bmp_header_construct(img);

    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (int32_t i = 0; i < header.biHeight; ++i) {
        for (int32_t j = 0; j < header.biWidth; ++j) {
            struct pixel pixel = {0};
            if (image_get_pixel_by_coordinates(img, header.biHeight - i - 1, j, &pixel) == IMAGE_OK) {
                if (!fwrite(&pixel, sizeof (struct pixel), 1, out)) {
                    return WRITE_ERROR;
                }
            } else return WRITE_ERROR;
        }
        if(!write_array(header, out)){
            return WRITE_ERROR;
        }

    }

    return WRITE_OK;
}

