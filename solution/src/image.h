//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include "marray_pixel.h"
#include "pixel.h"
#include "stdint.h"

struct image {
    int32_t width;
    int32_t height;
    struct marray_pixel* data;
};

enum image_status {
    IMAGE_OK = 0,
    IMAGE_INVALID_COORDINATES,
};

struct image* image_init (int32_t width, int32_t height);
enum image_status image_get_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel);
enum image_status image_set_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel);
void image_destroy (struct image* image);



#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
