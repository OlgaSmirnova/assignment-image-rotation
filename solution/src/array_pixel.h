//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ARRAY_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_ARRAY_PIXEL_H

#include "stdbool.h"
#include "stddef.h"
#include "stdint.h"

struct array_pixel {
    struct pixel* data;
    size_t size;
};

struct optional_array_pixel {
    struct array_pixel value;
    bool valid;
};

extern const struct optional_array_pixel NONE_ARRAY_PIXEL;

static inline void array_pixel_zero (struct array_pixel a);

struct optional_array_pixel array_pixel_init(size_t size);
struct optional_pixel array_pixel_get(struct array_pixel a, size_t i );
bool array_pixel_set(struct array_pixel a, size_t i, struct pixel value);
void array_pixel_free(struct array_pixel* a);

struct optional_array_pixel some_array_pixel(struct array_pixel array);

#endif //ASSIGNMENT_IMAGE_ROTATION_ARRAY_PIXEL_H
