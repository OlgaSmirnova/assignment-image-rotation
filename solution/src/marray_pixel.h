//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MARRAY_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_MARRAY_PIXEL_H

#include "array_pixel.h"
#include "stdbool.h"
#include "stdint.h"

struct marray_pixel {
    struct array_pixel* data;
    size_t size;
};

struct optional_marray_pixel {
    struct marray_pixel* value;
    bool valid;
};

extern const struct optional_marray_pixel NONE_MARRAY_PIXEL;

struct optional_marray_pixel marray_pixel_init(size_t w, size_t h);
struct optional_pixel marray_pixel_get(struct marray_pixel a, size_t i, size_t j);
bool marray_pixel_set(struct marray_pixel a, size_t i, size_t j, struct pixel value);
void marray_pixel_free(struct marray_pixel* array);

struct optional_marray_pixel some_marray_pixel(struct marray_pixel* array);

#endif //ASSIGNMENT_IMAGE_ROTATION_MARRAY_PIXEL_H
