//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#include <stdbool.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct optional_pixel {
    struct pixel value;
    bool valid;
};

extern const struct optional_pixel NONE_PIXEL;
extern const struct pixel PIXEL_WHITE;

struct optional_pixel some_pixel (struct pixel i);



#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
