//
// Created by etoxto on 28.12.2021.
//

#include "image.h"
#include "malloc.h"

struct image* image_init (const int32_t width, const int32_t height) {
    struct image* image = malloc(sizeof (struct image));
    if (image) {
        image->width  = width;
        image->height = height;
        struct optional_marray_pixel opt_marray = marray_pixel_init(height, width);
        if (opt_marray.valid) {
            image->data = opt_marray.value;
            return image;
        } else {
            free(image);
            return NULL;
        }
    }

    return image;
}

enum image_status image_get_pixel_by_coordinates (struct image const* image, const int32_t x, const int32_t y, struct pixel* pixel) {
    struct optional_pixel opt_pixel = marray_pixel_get(*image->data, x, y);
    if (opt_pixel.valid) {
        *pixel = opt_pixel.value;
        return IMAGE_OK;
    } else {
        return IMAGE_INVALID_COORDINATES;
    }
}

enum image_status image_set_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel) {
    if (marray_pixel_set(*image->data, x, y, *pixel)) {
        return IMAGE_OK;
    } else {
        return IMAGE_INVALID_COORDINATES;
    }
}

void image_destroy (struct image* image) {
    marray_pixel_free(image->data);
    free(image);
}
