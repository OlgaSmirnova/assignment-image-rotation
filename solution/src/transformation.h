//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H

#include "image.h"

struct image * rotate(struct image const source );

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
