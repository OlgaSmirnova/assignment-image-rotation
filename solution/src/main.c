#include <stdio.h>

#include "bmp.h"
#include "file.h"
#include "transformation.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: ./bmp_rotator <source_file> <destination_file>");
        return 1;
    }

    FILE *in = NULL;
    FILE *out = NULL;

    struct image* new_image = NULL;
    if (open_file(argv[1], &in, FILE_RB)) {
        struct image* image = NULL;
        if (from_bmp(in, &image) == READ_OK) {
            new_image = rotate(*image);
        }
        image_destroy(image);
        close_file(&in);
    }

    if (open_file(argv[2], &out, FILE_WB)) {
        to_bmp(out, new_image);
        image_destroy(new_image);
        close_file(&out);
    }

    return 0;
}
