//
// Created by etoxto on 28.12.2021.
//

#include "pixel.h"

const struct optional_pixel NONE_PIXEL = {0};
const struct pixel PIXEL_WHITE = {0};

struct optional_pixel some_pixel ( struct pixel i ) {
    return (struct optional_pixel) { .value = i, .valid = true };
}

