//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include "stdint.h"
#include "stdio.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfSignature;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

static inline bool bmp_signature_check (const struct bmp_header* header);
static inline bool bmp_to_image (FILE* file, const struct bmp_header* header, struct image** img);
static inline struct bmp_header bmp_header_construct (struct image const* img);

enum read_status from_bmp (FILE* in, struct image** img);
enum write_status to_bmp (FILE* out, struct image const* img);


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
