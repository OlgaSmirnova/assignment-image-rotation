//
// Created by etoxto on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILEIO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILEIO_H

#include <stdbool.h>
#include <stdio.h>

#define FILE_RB "rb"
#define FILE_WB "wb"

bool open_file (const char* filename, FILE** file, const char* mode);
bool close_file (FILE** file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILEIO_H
